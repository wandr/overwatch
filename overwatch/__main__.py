"""
Copyright:
2018 Wandr LLC; All Rights Reserved.

Description:
The wander daemon; runner script for interacting with wanders and their activities.
"""
from pkg_resources import iter_entry_points
from wandrlib.logger import Logger

LOG = Logger('overwatch')


def main() -> None:
    """
    Runtime method for the daemon.
    """
    LOG.info('Starting overwatch')
    watchers = {}
    for node_watcher in iter_entry_points('node.watchers'):
        watchers[node_watcher.name] = node_watcher.load()

    for watcher in watchers:
        LOG.info('Starting "%s" watcher', watcher)
        new_watcher = watchers[watcher]()
        new_watcher.run()
        LOG.info('Launched "%s" watcher', watcher)


if __name__ == '__main__':
    main()
