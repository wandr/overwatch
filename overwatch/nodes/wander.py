"""
Copyright:
2018 Wandr LLC; All Rights Reserved.

Description:
Wander abstraction.
"""
from copy import deepcopy

from wandrlib.api import connection
from wandrlib.logger import Logger

LOG = Logger(__name__)


class Builder(object):
    def __init__(self) -> None:
        """
        Initiates a Builder object with a persistent database object.
        """
        with connection() as api:
            self.db = api.pdb.database()

    def run(self) -> None:
        """
        Begins the stream.
        """
        self.db.child('wanders').stream(self.handler, stream_id=__name__)

    def handler(self, message: dict) -> None:
        """
        Creates wander abstractions for a given message and subsequently finds nearby places
        and a distance matrix.
        :param message: A dictionary containing expected, defined, Firebase keys.
        """
        path = message['path']
        parts = path.split('/')
        if message['event'] == 'patch':
            LOG.debug('Ignoring patch event on path: %s', path)
            return
        if path == '/':
            data = message['data']
        elif any(parts) and len(parts) == 2 and not message['data']:
            LOG.debug('Ignoring put event for "%s"; it smells like a "remove" event.', path)
            return
        elif any(parts) and len(parts) <= 3:
            data = {path: message['data']}
        else:
            LOG.debug('Omitting path %s', path)
            return
        if data is None:
            LOG.debug('No data for %s!', path)
            return
        LOG.debug('Processing %i nodes', len(data.items()))
        for source, data in data.items():
            source_parts = source.split('/')
            if len(source_parts) == 1:
                nid = source
            else:
                try:
                    nid = source_parts[1]
                except IndexError:
                    LOG.debug('Could not grab NID name from source "%s"', source)
                    continue
            self.process(nid, data)

    def process(self, nid: str, data: dict) -> None:
        """
        Processes a node, expecting it to be
        :param nid: Node ID (i.e. /wanders/$wid)
        :param data: The values under the node ID
        """
        if not data:
            node = self.db.child('wanders').child(nid).get()
            data = node.val()
        activities = data.get('activities')
        if not activities:
            LOG.debug('Resolving activities for %s', nid)
            nearby_criteria = {
                'location': data['location'],
                'radius': data['radius'],
            }
            with connection() as api:
                activities = api.nearby(data['placeTypes'], **nearby_criteria)
                data['map'] = None
        else:
            LOG.debug('Activities already defined for %s', nid)
        matrix = data.get('map')
        if not matrix or matrix.get('status') != 'OK':
            LOG.debug("Resolving matrix for %s's activities", nid)
            with connection() as api:
                matrix = api.matrix(activities, **{'mode': 'driving'})
        else:
            LOG.debug('Matrix already defined for %s', nid)
        if activities != data.get('activities') or matrix != data.get('map'):
            LOG.info("Updating %s's activities and map.", nid)
            new_data = deepcopy(data)
            new_data['activities'] = activities
            new_data['map'] = matrix
            self.db.child('wanders').child(nid).update(new_data)
        else:
            LOG.info('No activities or map changes for %s', nid)
