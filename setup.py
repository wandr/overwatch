"""
Copyright:
2018 Wandr LLC; All Rights Reserved.

Description:
An "overlord" for running various microservices on database nodes.
"""
from setuptools import find_packages
from setuptools import setup


def readme() -> str:
    """
    Returns the README.txt file for a long description.
    :returns: Read file as a string.
    """
    with open('README.md') as readme_file:
        return str(readme_file)


setup(
    author='Russell Bunch',
    author_email='russell@wandr.us',
    dependency_links=[
        "git+https://gitlab.com/wandr/wandrlib.git#egg=wandrlib",
    ],
    description='Microservices for data node interactions.',
    extras_require={
        'ci': [
            'tox',
        ],
        'lint': [
            'flake8',
        ],
        'unit': [
            'coverage',
            'nose',
        ],
    },
    entry_points={
        'console_scripts': [
            'overwatch=overwatch.__main__:main',
        ],
        'node.watchers': [
            'wander=overwatch.nodes.wander:Builder',
        ],
    },
    include_package_data=True,
    install_requires=[
        'wandrlib',
    ],
    long_description=readme(),
    maintainer='Wandr Team',
    maintainer_email='russell@wandr.us',
    name='Overwatch',
    packages=find_packages(),
    test_suite='nose.collector',
    url='https://gitlab.com/wandr/overwatch',
    version='2.0.0',
    zip_safe=False,
)
