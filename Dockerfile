FROM            registry.gitlab.com/rustydb/docker/python
MAINTAINER      russell@wandr.us

# Setup the virtual env directory.
RUN             pip install virtualenv && virtualenv /env

# Install the application.
ADD             . /app
WORKDIR         /app
RUN             LC_CTYPE="en_US.UTF-8" /env/bin/python setup.py install

# Launch Overwatch!
ENTRYPOINT      ["/env/bin/python", "-u"]
CMD             ["/env/bin/overwatch"]
