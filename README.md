# Overwatch

This application is responsible for running microservices on database locations.

[![coverage report](https://gitlab.com/wandr/overwatch/badges/master/coverage.svg)](https://gitlab.com/wandr/overwatch/commits/master)
[![pipeline status](https://gitlab.com/wandr/overwatch/badges/master/pipeline.svg)](https://gitlab.com/wandr/overwatch/commits/master)

### `wanderd` microservice

The `wanderd` MS is responsible for reading newly uploaded Wanders, finding 
relevant "places", and returning an aggregate to the user. 

### Credentials

For each API you want to authenticate with, provide the following:
```bash
# Firebase:
export GOOGLE_APPLICATION_CREDENTIALS=/path/to/serviceAccountKey.json

# Google Maps:
export GMAPS_API_KEY="<your key>"

# Our fork of Pyrebase (unofficial API):
export GOOGLE_APPLICATION_CREDENTIALS=/path/to/serviceAccountKey.json
export FIREBASE_DOMAIN="<firebase-app>"
export FIREBASE_DB="<firebase-db>"
export FIREBASE_API_KEY="<your key>"
```
